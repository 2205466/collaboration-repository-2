require 'nokogiri'
require_relative '../Objects/Student'
require_relative '../Objects/Professor'
require_relative '../Objects/Grades'



class XmlParser

  def initialize
    @prof_docu_path = 'resources/professors.xml'
    @professor_docu = Nokogiri::XML(File.open(@prof_docu_path))
    @student_doc_path = 'resources/students.xml'
    @student_doc = Nokogiri::XML(File.open(@student_doc_path))
    @formula_doc_path = 'resources/Formulas.xml'
    @formula_doc = Nokogiri::XML(File.open(@formula_doc_path))
    @all_students = []
    @all_professors = []
    populate_stud_n_prof
  end


  #method to populate @all_students and @all_professors list during init
  def populate_stud_n_prof
    @all_students = extract_student_details
    @all_professors = extract_professor_detail
  end

  def lol
    puts @all_students
  end

  def add_course_to_student( student_id, new_course_id, new_course_name, new_p_grade, new_m_grade, new_f_grade)

    #Load hte xml file
    doc = @student_doc

    # Find the id of the student that you want to add a course to
    # Then we get the student with that same id
    student = doc.at("//Student[@id='#{student_id}']")

    # This is just DOM parsing again
    # Create a new course element
    new_course = Nokogiri::XML::Node.new('course', doc)
    new_course['id'] = new_course_id

    course_name = Nokogiri::XML::Node.new('courseName', doc)
    course_name.content = new_course_name

    grades = Nokogiri::XML::Node.new('grades', doc)

    p_grade = Nokogiri::XML::Node.new('pgrade', doc)
    p_grade.content = new_p_grade
    m_grade = Nokogiri::XML::Node.new('mgrade', doc)
    m_grade.content = new_m_grade
    f_grade = Nokogiri::XML::Node.new('fgrade', doc)
    f_grade.content = new_f_grade

    # Add the child nodes to the new course element
    new_course.add_child(course_name)
    new_course.add_child(grades)
    grades.add_child(p_grade)
    grades.add_child(m_grade)
    grades.add_child(f_grade)

    # Find the "courses" element and add the new "course" as its child
    courses_element = student.at('courses')
    courses_element.add_child(new_course)


    File.write(@student_doc_path, doc.to_xml(indent: 2))
  end
  def remove_course(student_id, course_id)
    doc = @student_doc
    student = doc.at("//Student[@id='#{student_id}']")
    #for name  student = doc.at("//Student[name='#{student_name}']")
    course = student.at("//course[@id='#{course_id}']")

    # remove the course
    course.remove if course

    # Save the modified XML back to the file
    File.write(@student_doc_path, doc.to_xml)
  end
  def edit_grade(student_id, course_id, grade_type, new_grade)
    #Load hte xml file
    doc = @student_doc

    # The return  statements are there so that we don't get a error if what we are searching for
    # doesn't exist
    student = doc.at("//Student[@id='#{student_id}']")
    #for name  student = doc.at("//Student[name='#{student_name}']")
    return unless student

    # Find the course element by ID within the student's courses
    course = student.at(".//course[@id='#{course_id}']")
    return unless course

    # Find the grades element within the course
    grades = course.at('grades')
    return unless grades

    # update the  grade based on whether it's prelim, midterm or finals
    case grade_type
    when 'Prelim'
      p_grade = grades.at('pgrade')
      p_grade.content = new_grade if p_grade
    when 'Midterm'
      m_grade = grades.at('mgrade')
      m_grade.content = new_grade if m_grade
    when 'Finals'
      f_grade = grades.at('fgrade')
      f_grade.content = new_grade if f_grade
    end

    File.write(@student_doc_path, doc.to_xml(indent: 2))
  end

  def extract_student_details
    students_list = []

    # go through all the students
    @student_doc.xpath('/Students/Student').each do |student_tag|

      student_id = student_tag['id']
      student_name = student_tag.at('lastName').text + ", "+student_tag.at('firstName').text

      # Create a new Student object and add it to student array
      student = Student.new(student_id, student_name)

      # iterate through the student's courses
      student_tag.xpath('courses/course').each do |course_tag|
        course_id = course_tag['id']
        course_units = course_tag['units']
        course_name = course_tag.at('courseName').text

        # Extract grades
        p_grade = course_tag.at('grades/pgrade')&.text
        m_grade = course_tag.at('grades/mgrade')&.text
        f_grade = course_tag.at('grades/fgrade')&.text

        #add the course detail and their grades to the course array inside the student
        grade = Grades.new(self, course_id, course_name, course_units, p_grade.to_f, m_grade.to_f, f_grade.to_f)
        student.add_grade(grade)
      end #end of courses loop

      students_list << student
    end #end of students loop
    students_list #return list
  end

  def student_info(school_id)
    # Iterate through all the students
    @student_doc.xpath('/Students/Student').each do |student_tag|
      student_id = student_tag['id']
      student_name = student_tag.at('lastName').text + ", " + student_tag.at('firstName').text

      if student_id == school_id

        student = Student.new(student_id, student_name)

        # Iterate through the student's courses
        student_tag.xpath('courses/course').each do |course_tag|
          course_id = course_tag['id']
          course_units = course_tag['units']
          course_name = course_tag.at('courseName').text

          # Extract grades
          p_grade = course_tag.at('grades/pgrade')&.text
          m_grade = course_tag.at('grades/mgrade')&.text
          f_grade = course_tag.at('grades/fgrade')&.text

          # Add the course details and their grades to the course array inside the student
          grade = Grades.new(self, course_id, course_name, course_units, p_grade.to_f, m_grade.to_f, f_grade.to_f)
          student.add_grade(grade)
        end
        return student
      end
      return "what"
    end
  end


  #given a student id, this method will return the student object with the
  # corresponding student it
  def get_student_object_from_id(student_id)
    @all_students.each { |stodant|
      if stodant.id == student_id
        return stodant
      end
    }
  end

  def extract_professor_detail
    professor_list = []

    doc = @professor_docu

    # go through all the professors
    doc.xpath('//prof').each do |professor_tag|
      prof_id = professor_tag['id']
      prof_name = professor_tag.at('lastName').text + ", "+professor_tag.at('firstName').text

      # Create a new Student object and add it to student array
      professor = Professor.new(prof_id, prof_name)

      professor_list << professor

      units_list = []

      # iterate through the faculty's courses
      professor_tag.xpath('./units/code').each do |units|
        units_list << units.text
      end #end of units do loop

      # add units array to the prof
      professor.add_units(units_list)

    end #end of professors do loop
    professor_list #return list of professor objects
  end #end of method


  #given a professor id, this method will return the list of class codes of the professor
  def get_class_codes(professor_id)
    class_codes = []

    professor = @professor_docu.at("//prof[@id='#{professor_id}']")

    if professor
      class_codes = professor.xpath('.//units/code').map(&:text)
    else
      puts "Professor: #{professor_id} not found."
    end

    class_codes #return class codes list

  end #end get class codes method
  def get_students_all(professor_id)
    class_codes = get_class_codes(professor_id)

    for i in class_codes
      get_students_of_class(i)
    end
  end #end of get students method

  #todo
  def get_prof_list
    professor = @professor_docu.at("//prof[@id='#{professor_id}']")
    class_codes = professor.xpath('.//units/code').map(&:text)
  end

  #todo
  def print_all_professor_info
    prof_list = get_prof_list
    prof_list.each do |professor|

    end
  end

  #return ids of students
  def get_students_of_class(class_code)
    list_students = []
    xpath_expression = "//Student[.//course[@id='#{class_code}']]"
    students = @student_doc.xpath(xpath_expression)
    # return the list of matching students
    students.each do |student|
      student_id = student['id']
      list_students << student_id
    end
    list_students #return list of students
  end# end of get students of class method

  def login(id, password)

    student = @student_doc.at("//Student[@id='#{id}' and pass='#{password}']")
    return id if student

    professor = @professor_docu.at("//prof[@id='#{id}' and pass='#{password}']")
    return id if professor

    nil
  end

  #course id is  class code
  def get_course_weights(course_id)


    doc = @formula_doc

    course_element = doc.xpath("/Formulas/course[@code='#{course_id}']").first

    prelim_weight = course_element.at('prelimWeight').text.to_f
    midterm_weight = course_element.at('midtermWeight').text.to_f
    finals_weight = course_element.at('finalWeight').text.to_f

    { prelim_weight: prelim_weight, midterm_weight: midterm_weight, finals_weight: finals_weight }
  end

  def get_all_grades_class(class_code)
    grades = []

    doc = @student_doc

    doc.xpath('/Students/Student').each do |student_tag|
      student_id = student_tag['id']
      temp = get_grades_student(class_code.to_s, student_id)
      grades << temp if temp
    end
    grades
  end#end method

  #TODO FIX WHITESPACES
  def get_grades_student(class_code, id_number)
  doc = @student_doc

    doc.xpath('/Students/Student').each do |student_tag|
      student_id = student_tag['id']

      if student_id == id_number
        # Iterate through the courses for this student
        student_tag.xpath('courses/course').each do |course_tag|
          course_id = course_tag['id'].to_i
          course_units = course_tag['units'].to_i
          course_name = course_tag.at('courseName').text
          grades_tag = course_tag.at('grades')

          # Extract the grades for the specified class code
          if course_id.to_s == class_code
            prelim_grade = grades_tag.at('pgrade').text.to_i
            midterm_grade = grades_tag.at('mgrade').text.to_i
            final_grade = grades_tag.at('fgrade').text.to_i

            grade = Grades.new(self, course_id, course_name, course_units, prelim_grade.to_f, midterm_grade.to_f, final_grade.to_f)
            return grade
          end
        end
      end
    end
  return nil
  end

  def get_my_grades(student_id)
    grades = []

    doc = @student_doc

    doc.xpath('/Students/Student').each do |student_tag|
      current_student_id = student_tag['id']

      # Check if the current student's ID matches the provided student_id
      if current_student_id == student_id
        # Iterate through the courses for this student
        student_tag.xpath('courses/course').each do |course_tag|
          course_id = course_tag['id'].to_i
          course_units = course_tag['units'].to_i
          course_name = course_tag.at('courseName').text
          grades_tag = course_tag.at('grades')

          # Extract the grades for each course
          prelim_grade = grades_tag.at('pgrade').text.to_i
          midterm_grade = grades_tag.at('mgrade').text.to_i
          final_grade = grades_tag.at('fgrade').text.to_i

          # Create a Grades object for each course and add it to the grades array
          grade = Grades.new(self, course_id, course_name, course_units, prelim_grade.to_f, midterm_grade.to_f, final_grade.to_f)
          grades << grade
        end
        break
      end
    end

    return grades
  end

  def get_enrolled_courses(student_id)
    enrolled_courses = []

    # Iterate through the students
    @student_doc.xpath('/Students/Student').each do |student_tag|
      current_student_id = student_tag['id']

      # Check if the current student's ID matches the provided student_id
      if current_student_id == student_id
        student_name = student_tag.at('lastName').text + ', ' + student_tag.at('firstName').text
        puts "\nENROLLED COURSES:"

        # Iterate through the student's courses
        student_tag.xpath('courses/course').each_with_index do |course_tag, index|
          course_name = course_tag.at('courseName').text
          course_units = course_tag['units']
          enrolled_courses << "(#{index + 1}) #{course_name} - #{course_units}"
        end
        break
      end
    end

    if enrolled_courses.empty?
      "No enrolled courses found for Student ID #{student_id}."
    else
      enrolled_courses.join("\n")
    end
  end


end