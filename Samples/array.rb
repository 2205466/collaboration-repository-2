puts "---------Array in Loop--------"
arr = ["Gyarados", "Blastoise", "Charizard"]
for i in arr do
  puts i
end

puts "---------Do While Loop--------"

loop do
  puts "Ruby Mine"
  val = '7'


  if val == '7'
    break
  end
end

puts "---------Until Loop--------"

var = 7
until var == 11 do
  puts var * 10
  var = var + 1
end