class Person
	def initialize(id, firstname, lastname)
		@@id = id
		@@firstname = firstname
		@@lastname = lastname
	end

	def id
		@@id
	end

	def firstname
		@@firstname
	end

	def lastname
		@@lastname
	end
end

studentLogTest = Person.new(2225189, "Rey", "Agbayani")
puts studentLogTest.id
print studentLogTest.firstname + "\s"
puts studentLogTest.lastname