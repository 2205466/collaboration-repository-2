require 'nokogiri'

puts"ID: "
id = gets.chomp
puts"Password: "
pass = gets.chomp

builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
  xml.root {
    xml.products {
      xml.widget {
        xml.id "#{id}"
        xml.password "#{pass}"
      }
    }
  }
end
puts builder.to_xml

